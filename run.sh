#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OUTDIR=${OUTDIR:-$SCRIPTDIR/run}
UNSERVED_SRC=${UNSERVED_SRC:-$SCRIPTDIR/unserved-testbench}
UNSERVED_JAR=${UNSERVED_JAR:-$UNSERVED_SRC/target/unserved-testbench-1.0-SNAPSHOT.jar}

WSC_CONVERTED="$SCRIPTDIR/wsc-converted"
WSC_ORIGINAL="$SCRIPTDIR/sws-test-collections/src/main/resources/services/wsc08/"
CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.WscRawExperimentRunner
PP_CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design.PPRawExperimentDesignRunner
WSCCONVERTER_CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.WscProblemConverterApp

if [ ! -z "$1" ]; then
  OUTDIR=$1
fi

if [ ! -d "${OUTDIR}" ]; then
  mkdir -p $OUTDIR
fi

if [ ! -d "${OUTDIR}" ]; then
  echo "${OUTDIR} is not a directory!" 
  exit 1
fi
OUTDIR=$(realpath "${OUTDIR}")

# --- fix a cpu clock speed
set-cpufreq.sh && CLOCKSET=1 || echo "Failed to set a cpu clock speed, ignoring"

# --- download unserved-testbench, if needed
if [ ! -d "${UNSERVED_SRC}" ]; then
    pushd "${SCRIPTDIR}"
    git clone --depth 1 https://bitbucket.org/alexishuf/unserved-testbench.git || exit 1
    pushd unserved-testbench
    git config submodule."src/main/resources/unserved".url http://bitbucket.org/alexishuf/unserved.git
    git submodule update --init --recursive
    popd
    UNSERVED_SRC="${SCRIPTDIR}/unserved-testbench"
    UNSERVED_JAR="${UNSERVED_SRC}/target/unserved-testbench-1.0-SNAPSHOT.jar"
    popd
fi
   

# --- package unserved-testbench
if [ ! -z "${UNSERVED_SRC}" ]; then
  pushd "${UNSERVED_SRC}"
  mvn package || exit 1
  popd
  sleep 1s
fi

# --- convert WSC'08 problems, if needed
if [ ! -d "${WSC_CONVERTED}" ]; then
    if [ ! -d "${WSC_ORIGINAL}" ]; then
	echo "WSC'08 files not found at ${WSC_ORIGINAL}, downloading..."
	pushd "${SCRIPTDIR}"
	git clone --depth 1 "https://github.com/kmi/sws-test-collections.git" || exit 1
	WSC_ORIGINAL="${SCRIPTDIR}/sws-test-collections/src/main/resources/services/wsc08"
	popd
    fi

    echo "uNSERVED WSC'08 files not found at ${WSC_CONVERTED}, converting..."
    mkdir -p "${WSC_CONVERTED}" || exit 1
    java -cp "${UNSERVED_JAR}" $WSCCONVERTER_CLASSNAME --wsc-root "${WSC_ORIGINAL}" \
	 --out-root "${WSC_CONVERTED}"
    if [ ! $? -eq 0 ]; then
	echo "Failed to convert WSC'08 problems"; exit 1
    fi
fi

# --- run tests

pushd "${UNSERVED_SRC}"
git log -1 > "${OUTDIR}/unserved-testbench.commit"
popd

java -cp "${UNSERVED_JAR}" $CLASSNAME --design-csv "${SCRIPTDIR}/iscc.csv" \
    --results-csv "${OUTDIR}/results.csv" \
    --result-objects-dir "${OUTDIR}/results" --continue "${OUTDIR}/state.json" 2>&1 \
  | tee -a "${OUTDIR}/log"

java -cp "${UNSERVED_JAR}" ${PP_CLASSNAME} --design-csv "${SCRIPTDIR}/iscc-pp.csv" \
    --results-csv "${OUTDIR}/results-pp.csv" \
    --result-objects-dir "${OUTDIR}/results-pp" --continue "${OUTDIR}/state-pp.json" 2>&1 \
  | tee -a "${OUTDIR}/log-pp"

# --- restore cpu clock
if [ ! -z "${CLOCKSET}" ]; then
   unset-cpufreq.sh
fi

# --- process data
pushd "${SCRIPTDIR}/iscc"
R --no-save < iscc.R
popd



