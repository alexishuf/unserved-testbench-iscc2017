options(digits = 4, width = 100) #set precision for float values and and chars by line
require("nlstools");

setwd(getSrcDirectory()[1])
cases <- read.csv(file = "../iscc.csv",na.strings = c("NA",""," "),header=TRUE);
casespp <- read.csv(file = "../iscc-pp.csv",na.strings = c("NA",""," "),header=TRUE);
composit <- read.csv(file = "../composit-times.csv",na.strings = c("NA",""," "),header=TRUE);
results <- read.csv(file = "../run/results.csv",na.strings = c("NA",""," "),header=TRUE);
results$name <- seq.int(nrow(results));
resultspp <- read.csv(file = "../run/results-pp.csv",na.strings = c("NA",""," "),header=TRUE);
resultspp$name <- seq.int(nrow(resultspp));
df <- merge(cases, results, by='name');
dfpp <- merge(casespp, resultspp, by='name');

diag <- function (fit, x, y) {
  print(summary(fit));
  par(mfrow=c(2, 2))
  plot(fit)
  par(mfrow=c(1, 1))
}
diagnls <- function (fit, x, y) {
  print(summary(fit));
  par(mfrow=c(2, 2));
  plot(nlsResiduals(fit));
  par(mfrow=c(1, 1));
}
plot_predict <- function(fit, x, y) {
  plot(x,y,pch=19)
  lines(min(x):max(x), predict(fit, data.frame(x=min(x):max(x))), col="red")
}

###############

##################################
# DStarLiteLayeredIO models
##################################

x <- c();
y <- c();
for (rn in subset(dfpp, algorithm == "DStarLiteLayeredIO" & ioCount == 3 & nonDummyChain < 0)[,"run.no.in.std.order"]) {
  x <- c(x, subset(dfpp, run.no.in.std.order == rn)[1,"chainLength"]);
  # look back 2 poistions in the standard test order to get the test, from this same replication that has the same chainLength but an ioCount of 1
  y <- c(y, subset(dfpp, run.no.in.std.order == rn)[1,"composition"] - subset(dfpp, run.no.in.std.order == rn-2)[1,"composition"]);
}
plot(x,y,pch=19)
fit <- lm(y~poly(x,2));
diag(fit, x, y); #non-uniform residuals, not normal
plot_predict(fit, x, y);

# log transform on x and y
x <- c();
y <- c();
for (rn in subset(dfpp, algorithm == "DStarLiteLayeredIO" & ioCount == 3 & nonDummyChain < 0)[,"run.no.in.std.order"]) {
  x <- c(x, subset(dfpp, run.no.in.std.order == rn)[1,"chainLength"]);
  # Apply the log2 transform on the original absolute times. set y to the difference of the logarithms. This avoids NaNs
  y <- c(y, log2(subset(dfpp, run.no.in.std.order == rn)[1,"composition"]) - log2(subset(dfpp, run.no.in.std.order == rn-2)[1,"composition"]));
}
x <- log2(x);
plot(x,y,pch=19)
fit <- lm(y~x);
diag(fit, x, y); #uniform residuals, is it normal?
plot_predict(fit, x, y);

# log transform on x and y (transform the difference only)
x <- c();
y <- c();
for (rn in subset(dfpp, algorithm == "DStarLiteLayeredIO" & ioCount == 3 & nonDummyChain < 0)[,"run.no.in.std.order"]) {
  x <- c(x, subset(dfpp, run.no.in.std.order == rn)[1,"chainLength"]);
  y <- c(y,  subset(dfpp, run.no.in.std.order == rn)[1,"composition"] - subset(dfpp, run.no.in.std.order == rn-2)[1,"composition"]);
}
x <- log2(x);
# There are negative differences, so shift our zero, and add a small factor to avoid a -Inf
b <- abs(min(c(min(y), 0)))+0.001;
y <- log2(y+b);
# fy = l2(y+b)  # 2**fy = y+b  # y = 2**fy-b
plot(x,y,pch=19)
fit <- lm(y~x);  #bad model
diag(fit, x, y); #non-uniform residuals, normal with outliers
plot(x,y,pch=19)
#Undo the b trick when plotting
lines(min(x):max(x), (function(fy) 2**fy-b)(predict(fit, data.frame(x=1:10))), col="red")

# log transform on x and y (transform the difference only), removing upper and lower 5%
x <- c();
y <- c();
for (rn in subset(dfpp, algorithm == "DStarLiteLayeredIO" & ioCount == 3 & nonDummyChain < 0)[,"run.no.in.std.order"]) {
  x <- c(x, subset(dfpp, run.no.in.std.order == rn)[1,"chainLength"]);
  y <- c(y,  subset(dfpp, run.no.in.std.order == rn)[1,"composition"] - subset(dfpp, run.no.in.std.order == rn-2)[1,"composition"]);
}
x <- log2(x);
# Remove samples in the lower and upper 5% quantiles
idxs <- which(y >= quantile(y, 0.05) & y <= quantile(y, 0.95));
# There are negative differences, so shift our zero, and add a small factor to avoid a -Inf
b <- abs(min(c(min(y), 0)))+0.001;
x <- x[idxs];
y <- y[idxs];
y <- log2(y+b);
# fy = l2(y+b)  # 2**fy = y+b  # y = 2**fy-b
plot(x,y,pch=19)
fit <- lm(y~x);  #bad model
diag(fit, x, y); #non-uniform residuals, has outliers
plot(x,y,pch=19)
#Undo the b trick when plotting
lines(min(x):max(x), (function(fy) 2**fy-b)(predict(fit, data.frame(x=1:10))), col="red")


##################################
# RESTdescPP models
##################################

x <- c();
y <- c();
for (rn in subset(dfpp, algorithm == "RESTdescPP" & ioCount == 3 & nonDummyChain < 0)[,"run.no.in.std.order"]) {
  x <- c(x, subset(dfpp, run.no.in.std.order == rn)[1,"chainLength"]);
  y <- c(y, subset(dfpp, run.no.in.std.order == rn)[1,"composition"] - subset(dfpp, run.no.in.std.order == rn-2)[1,"composition"]);
}
plot(x,y,pch=19)
fit <- lm(y~poly(x,2));
diag(fit, x, y); #not so normal, non-uniform residuals
plot_predict(fit, x, y); #looks legit

# log2 tranform on x and y
x <- c();
y <- c();
for (rn in subset(dfpp, algorithm == "RESTdescPP" & ioCount == 3 & nonDummyChain < 0)[,"run.no.in.std.order"]) {
  x <- c(x, subset(dfpp, run.no.in.std.order == rn)[1,"chainLength"]);
  y <- c(y, log2(subset(dfpp, run.no.in.std.order == rn)[1,"composition"]) - log2(subset(dfpp, run.no.in.std.order == rn-2)[1,"composition"]));
}
x <- log2(x);
plot(x,y,pch=19)
fit <- lm(y~poly(x,2));
diag(fit, x, y); #normal, uniformly distributed residuals
plot_predict(fit, x, y); #looks legit


x <- c();
y <- c();
for (rn in subset(dfpp, algorithm == "RESTdescPP" & ioCount == 3 & nonDummyChain < 0)[,"run.no.in.std.order"]) {
  x <- c(x, subset(dfpp, run.no.in.std.order == rn)[1,"chainLength"]);
  y <- c(y, subset(dfpp, run.no.in.std.order == rn)[1,"composition"] - subset(dfpp, run.no.in.std.order == rn-2)[1,"composition"]);
}
x <- log2(x);
