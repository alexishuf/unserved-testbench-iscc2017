uNSERVED experiments for IEEE ISCC 2017
======================================

This repository contains experiments data for the paper "Planning and Execution of Heterogeneous Service Compositions" by Alexis Huf, Ivan Salvadori and Frank Siqueira, submitted to IEEE ISCC 2017. There are three types of things here:

* `run.sh` is a do-it-all script to download & compile code, run the experiments and process the data;
* `run` has the raw experiment data;
* `iscc` directory contains processed data and the R script that generated it.

The `run` and `iscc` dirs come with the data shown in the submitted paper, executing `run.sh`will overwrite it. 

Running
-------

First, check the following requirements:

* At least 600M of free disk space
* Enough free RAM to spawn a JVM with 4096M maximum heap
* JDK >= 1.8
* Maven 3
* R (tested on 3.3.2)
* Git
* A Non-ancient bash
* A Internet connection and a some time
* If you want to set a CPU frequency cap, do it on `set-cpufreq.sh` and `unset-cpufreq.sh` scripts somewhere on your `$PATH`

To get started, simply type `./run.sh` and wait (a lot). The following will happen:

* [unserved-testbench][1] will be downloaded (to this directory) compiled, tested and packaged as an über-jar
* [sws-test-collections][2] will be downloaded (WSC'08 XML files come from here)
* WSC'08 problems will be converted to uNSERVED and saved under `wsc-converted`, this takes a while, specially number 8
* The experiments will run from the list in iscc.csv. This took ~130 minutes on the machine reported on the paper
* Data on run/ will be processed by `iscc/iscc.R`

[1]: https://bitbucket.org/alexishuf/unserved-testbench
[2]: https://github.com/kmi/sws-test-collections